#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x73064479, "struct_module" },
	{ 0x95be4f07, "input_register_device" },
	{ 0x701d0ebd, "snprintf" },
	{ 0x7681ce56, "usb_alloc_urb" },
	{ 0x802ef5ac, "usb_buffer_alloc" },
	{ 0x64cd5d16, "init_waitqueue_head" },
	{ 0xaaaed6c1, "input_allocate_device" },
	{ 0x972574c8, "kmem_cache_alloc" },
	{ 0x2c9b319a, "kmalloc_caches" },
	{ 0x54bf4158, "usb_register_driver" },
	{ 0x642e54ac, "__wake_up" },
	{ 0x52205f37, "input_event" },
	{ 0xf9a482f9, "msleep" },
	{ 0x12ec3d6a, "usb_unlink_urb" },
	{ 0x71356fba, "remove_wait_queue" },
	{ 0xd62c833f, "schedule_timeout" },
	{ 0x650fb346, "add_wait_queue" },
	{ 0x4b07e779, "_spin_unlock_irqrestore" },
	{ 0x712aa29b, "_spin_lock_irqsave" },
	{ 0xffd5a395, "default_wake_function" },
	{ 0x37afe3a2, "per_cpu__current_task" },
	{ 0xa3fa60da, "usb_submit_urb" },
	{ 0xb72397d5, "printk" },
	{ 0xccceaf8e, "dev_driver_string" },
	{ 0x3f1899f1, "up" },
	{ 0x748caf40, "down" },
	{ 0x37a0cba, "kfree" },
	{ 0x87f193e4, "input_free_device" },
	{ 0x4dc3829f, "usb_buffer_free" },
	{ 0xe3562661, "usb_free_urb" },
	{ 0xda2fe664, "input_close_device" },
	{ 0xde9922ef, "input_unregister_device" },
	{ 0x8bb8692c, "usb_kill_urb" },
	{ 0x8a1c739e, "usb_deregister" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("usb:v08D3p0001d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0525pA4A0d*dc*dsc*dp*ic*isc*ip*");

MODULE_INFO(srcversion, "1F1FA36498B0D174EBDB8DF");

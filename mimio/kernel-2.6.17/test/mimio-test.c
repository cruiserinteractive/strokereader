
/*
 * This is a bare-bones application that consumes events produced by
 * the mimio driver.  It is provided purely for informative purposes.
 * Mimicing the code herein will likely permit the creation of a full-service
 * mimio application.
 */
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <linux/input.h>

/*
 * These values are not arbitrary.  Do not modify them unless the
 * corresponding code for the mapping of hardware events to input
 * events has been modified in the mimio driver.
 */
#define MIMIO_EV_SYNC	(EV_SYN)
#define MIMIO_EV_ACTION	(EV_KEY)
#define MIMIO_EV_COORD	(EV_ABS)

#define MIMIO_AXIS_X	(ABS_X)
#define MIMIO_AXIS_Y	(ABS_Y)

#define MIMIO_INSTR_MIN		(BTN_TOOL_PEN)
#define MIMIO_PEN_BLACK		(MIMIO_INSTR_MIN + 0)
#define MIMIO_PEN_BLUE		(MIMIO_INSTR_MIN + 1)
#define MIMIO_PEN_GREEN		(MIMIO_INSTR_MIN + 2)
#define MIMIO_PEN_RED		(MIMIO_INSTR_MIN + 3)
#define MIMIO_BIG_ERASER	(MIMIO_INSTR_MIN + 4)
#define MIMIO_LIL_ERASER	(MIMIO_INSTR_MIN + 5)
/* mimio interactive */
#define MIMIO_PEN_INTERACTIVE	(MIMIO_INSTR_MIN + 6)
#define MIMIO_INTERACTIVE_B1	(MIMIO_INSTR_MIN + 7)
#define MIMIO_INTERACTIVE_B2	(MIMIO_INSTR_MIN + 8)
#define MIMIO_EXTRA_PENS	(MIMIO_INSTR_MIN + 9)

#define MIMIO_INSTR_MAX		(MIMIO_EXTRA_PENS)

#define MIMIO_BTN_MIN			(BTN_MISC)
#define MIMIO_BTN_MEMRESET		(MIMIO_BTN_MIN + 0)
#define MIMIO_BTN_NEWPAGE		(MIMIO_BTN_MIN + 1)
#define MIMIO_BTN_TAGPAGE		(MIMIO_BTN_MIN + 2)
#define MIMIO_BTN_PRINTPAGE		(MIMIO_BTN_MIN + 3)
#define MIMIO_BTN_MAXIMIZE		(MIMIO_BTN_MIN + 4)
#define MIMIO_BTN_FINDCTLPNL	(MIMIO_BTN_MIN + 5)
#define MIMIO_BTN_MAX			(MIMIO_BTN_FINDCTLPNL)

#define IsValidBtn(x) ((x) >= MIMIO_BTN_MIN && (x) <= MIMIO_BTN_MAX)
#define IsValidInstr(x) ((x) >= MIMIO_INSTR_MIN && (x) <= MIMIO_INSTR_MAX)
 
static
char *
mimio_event_decode(event)
	struct input_event * event;
{
	char * p;
	static char buf[512] = { 0 };
	static const char * const instrs[] = {
		"black pen",
		"blue pen",
		"green pen",
		"red pen",
		"big eraser",
                "lil eraser",
                "mimio interactive",
                "interactive btn1",
                "interactive btn2",
                "extra pens"
        };
	static const char * const btns[] = {
		"mem-reset",
		"new page",
		"tag page",
		"print page",
		"maximize",
		"find control panel"
	};

	if (event == NULL) {
		return "NULL event";
	}

	sprintf(buf, "time: 0x%x\n", event->time);
	p = buf + strlen(buf);

	switch (event->type) {
		case MIMIO_EV_SYNC:
			sprintf(p, "sync.\n");
			break;
		case MIMIO_EV_ACTION:
			/* determine the actor */
			if (IsValidInstr(event->code))
				sprintf(p, "%s ", instrs[event->code - MIMIO_INSTR_MIN]);
			else if (IsValidBtn(event->code))
				sprintf(p, "%s ", btns[event->code - MIMIO_BTN_MIN]);
			else
				sprintf(p, "invalid instr: 0x%x.\n", event->code);
			p = buf + strlen(buf);

			/* determine the action */
			if (event->value)
				sprintf(p, "pressed.\n");
			else
				sprintf(p, "released.\n");
			break;
		case MIMIO_EV_COORD:
			if (event->code == MIMIO_AXIS_X)
				sprintf(p, "x-axis: 0x%x.\n", event->value);
			else if (event->code == MIMIO_AXIS_Y)
				sprintf(p, "y-axis: 0x%x.\n", event->value);
			else 
				sprintf(p, "invalid axis: 0x%x.\n", event->code);
			break;
		default:
			sprintf(p, "invalid event.type: 0x%x.\n", event->type);
			break;
	}

	strcat(buf, "***\n");
	return buf;
}

int
main(argc, argv)
	int argc;
	char ** argv;
{
	char * p;
	FILE * pf;
	int i, k, fd;
	struct input_event event;

	pf = fopen("mimio-test.log", "w");

	if ((fd = open("/dev/input/event2", O_RDONLY)) < 0) {
		perror("/dev/input/event2");
		fprintf(pf, "bailing.\n");
		fprintf(stdout, "bailing.\n");
		fclose(pf);
		return fd;
	}

	if (argc <= 1)
		k = 0;
	else
		k = atoi(argv[1]);

	for (i=0; k == 0 || i < k; i++) {
		/*
		 * Read an event from the mimio.
		 */
		read(fd, &event, sizeof(struct input_event));

		/*
		 * Print out the undecoded event data.
		 */
		fprintf(stdout, "event %d:\n", i);
		fprintf(stdout, "   event.time: 0x%08x.\n", event.time);
		fprintf(stdout, "   event.type: 0x%08x.\n", event.type);
		fprintf(stdout, "   event.code: 0x%08x.\n", event.code);
		fprintf(stdout, "   event.value: 0x%08x.\n", event.value);
		fprintf(pf, "event %d:\n", i);
		fprintf(pf, "   event.time: 0x%08x.\n", event.time);
		fprintf(pf, "   event.type: 0x%08x.\n", event.type);
		fprintf(pf, "   event.code: 0x%08x.\n", event.code);
		fprintf(pf, "   event.value: 0x%08x.\n", event.value);

		/*
		 * Print out the decoded event data.
		 */
		p = mimio_event_decode(&event);
		fprintf(pf, p);
		fflush(pf);
		fprintf(stdout, p);
		fflush(stdout);

		/*
		if (k && i >= k)
			break;
		*/
	}

	close(fd);
	close(pf);

	return 0;
}


/* $Id$ */
#include "mimiolinux.h"

/**\file mimiolinux.cpp
 * Definitions for MimioLinux class
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#include <SDL_thread.h>
#include <stdio.h>
#include <typeinfo>
#include <vector>

#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>

#include "mimiodefines.h"
#include "schedlock.h"

/// Greater than this and we say we have wrapped around
#define WRAPAROUND_LIMIT 20000

/// function to unwrap -- negative numbers are too complicated
/// for now, so just return negative
#define wraparound(_val) (0)

namespace {
    int readthread(void* data) {
        MimioLinux *ml = static_cast<MimioLinux*>(data);
        return ml->private_run_thread();
    }
    typedef std::vector<MimioLinux::FALLBACK_FUNC> FALLBACK_VEC;
    FALLBACK_VEC *fallbacks = 0;
    bool CALLBACKS_DISABLED = false;
}
int MimioLinux::Init::count = 0;
MimioLinux::Init::Init() {
    if (count++ == 0) {
        fallbacks = new FALLBACK_VEC;
    }
}
MimioLinux::Init::~Init() {
    if (--count == 0) {
        delete fallbacks;
    }
}

void MimioLinux::add_fallback(FALLBACK_FUNC f) {
    if (!CALLBACKS_DISABLED)
        fallbacks->push_back(f);
}

void MimioLinux::set_fallback(FALLBACK_FUNC f) {
    fallbacks->clear();
    fallbacks->push_back(f);
    CALLBACKS_DISABLED = true;
}

MimioLinux::MimioLinux(DISPATCH_FUNC dispatcher, const char* device)
: active_fallback(0), thr(0), dispatch(dispatcher), fd(0),
penid(-1), lastx(-1), lasty(-1), gotevent(false),
penstate(MimioEvent::NOTHING)
{
    if ((fd = open(device, O_RDONLY)) < 0) {
        perror(device);
        fprintf(stderr, "[MIMIOLINUX] MimioLinux bailing (%s: %m).\n", device);
        return;
    }
    fprintf(stderr, "[MIMIOLINUX] Successfully opened %s for reading. Starting thread.\n", device);
    thr = SDL_CreateThread(readthread, this);
}

void MimioLinux::stop() {
    if (this && fd > 0) {
        fprintf(stderr, "[MIMIOLINUX] Closing file descriptor %d\n", fd);
        close(fd);
        fd = 0;
    }
}

MimioLinux::~MimioLinux() {
    stop();
}

int MimioLinux::join() {
    if (thr) {
        int retval = -1;
        stop();
        //SDL_WaitThread(thr, &retval);
        SDL_KillThread(thr);
        return retval;
    }
    return -1;
}

int MimioLinux::private_run_thread() {
    struct input_event event;
    ssize_t nread;
    int res = -2;
    fprintf(stderr, "[MIMIOLINUX] Blocking for first event read from descriptor %d\n", fd);
    while ((nread = ::read(fd, &event, sizeof(event))) > 0) {
        //fputs(".\n", stderr);
        if (!(res = decode(&event)))
            break;
    }
    fprintf(stderr,
            "[MIMIOLINUX] Done reading from descriptor %d (nread = %u, res = %d)\n",
            fd, (unsigned)nread, res);
    stop();
    return 0;
}

void MimioLinux::dispatch_coord() {
    static const MimioEvent::PENSTATE NEXT_STATE[] = {
        MimioEvent::NOTHING, /* NOTHING */
        MimioEvent::NOTHING, /* UP */
        MimioEvent::MOVING, /* DOWN */
        MimioEvent::MOVING  /* MOVING */
    };
    mimio_incr();
    MimioEvent *nme = new MimioEvent(penstate,
                                     lastx, lasty, penid);
    //fprintf(stderr, ".");
    if (!gotevent) {
	fprintf(stderr, "[MIMIOLINUX] Dispatching to %s@%p\n", typeid(dispatch).name(), reinterpret_cast<void*>(dispatch));
    }
    dispatch(nme);
    penstate = NEXT_STATE[penstate];
}

int MimioLinux::handle_coord(const struct input_event * event) {
    if (!gotevent) {
	fprintf(stderr, "[MIMIOLINUX] First event on fd %d is a coordinate\n", fd);
    }
    if (event->code == MIMIO_AXIS_X) {
        //sprintf(p, "(%d, %d), ", event->value, lasty);
        
        lastx = event->value < WRAPAROUND_LIMIT
            ? event->value
            : wraparound(event->value);

    } else if (event->code == MIMIO_AXIS_Y) {
        //sprintf(p, "(%d, %d), ", lastx, event->value);
        lasty = event->value < WRAPAROUND_LIMIT
            ? event->value
            : wraparound(event->value);
    } else {
        fprintf(stderr, "[MIMIOLINUX] invalid axis: 0x%x.\n", event->code);
        return 0;
    }
    if (penid >= 0 && lastx >= 0 && lasty >= 0) {
        // dispatch coord only when pendown received
        //dispatch_coord();
    }
    return 1;
}

int MimioLinux::handle_action(const struct input_event * event) {
    if (!gotevent) {
	fprintf(stderr, "[MIMIOLINUX] First event on fd %d is an action\n", fd);
    }
#if 0
    /* determine the actor */
    if (IsValidInstr(event->code))
        sprintf(p, "\n%s ", instrs[event->code - MIMIO_INSTR_MIN]);
    else if (IsValidBtn(event->code))
        sprintf(p, "\n%s ", btns[event->code - MIMIO_BTN_MIN]);
    else
        sprintf(p, "\ninvalid instr: 0x%x.\n", event->code);
    p = buf + strlen(buf);

    /* determine the action */
    if (event->value)
        sprintf(p, "pressed.\n");
    else
        sprintf(p, "released.\n");
#endif
    switch (event->code) {
    case MIMIO_PEN_BLACK:
    case MIMIO_PEN_BLUE:
    case MIMIO_PEN_GREEN:
    case MIMIO_PEN_RED:
    case MIMIO_BIG_ERASER:
    case MIMIO_LIL_ERASER:
        /* mimio interactive */
    case MIMIO_PEN_INTERACTIVE:
    case MIMIO_INTERACTIVE_B1:
    case MIMIO_INTERACTIVE_B2:
    case MIMIO_EXTRA_PENS:
        //fprintf(stderr, "mimio pen event\n");
        penid = event->code - MIMIO_INSTR_MIN;
        if (event->value) {
            if (penstate != MimioEvent::MOVING)
                penstate = MimioEvent::DOWN;
            /* we get coordinates BEFORE the pendown event now, so dispatch */
            dispatch_coord();
        } else {
            penstate = MimioEvent::UP;
            dispatch_coord();
        }
        break;
    case MIMIO_BTN_MEMRESET:
    case MIMIO_BTN_NEWPAGE:
    case MIMIO_BTN_TAGPAGE:
    case MIMIO_BTN_PRINTPAGE:
    case MIMIO_BTN_MAXIMIZE:
    case MIMIO_BTN_FINDCTLPNL:
        /* we don't to anything with these yet */
        break;
    default:
        return 0; /* probably not a mimio event */
        break;
    }
    return 1;
}


int MimioLinux::decode(const struct input_event * event) {
    int res = 0;
    if (event == NULL) {
        fprintf(stderr, "[MIMIOLINUX] Received a NULL event.. ignoring\n");
        return 1;
    }
    if (active_fallback) {
        return active_fallback(event, this);
    }

    switch (event->type) {
    case MIMIO_EV_SYNC:
        res = 1;
        break;
    case MIMIO_EV_ACTION:
        res = handle_action(event);
        break;
    case MIMIO_EV_COORD:
        res = handle_coord(event);
        break;
    }

    if (!res) {
        fprintf(stderr, "[MIMIOLINUX] invalid event.type: 0x%x or event.code: 0x%x for Mimio\n", event->type, event->code);
        if (/*!gotevent && */!fallbacks->empty()) {
            fprintf(stderr, "[MIMIOLINUX] Trying fallbacks.\n");
            for (FALLBACK_VEC::iterator it = fallbacks->begin(); it != fallbacks->end(); ++it) {
                if ((res = (**it)(event, this))) {
                    gotevent = true;
                    active_fallback = *it;
                    fprintf(stderr, "[MIMIOLINUX] Using fallback function.\n");
                }
            }
        }
        return res;
    }
    if (res && !gotevent) {
	fprintf(stderr, "[MIMIOLINUX] Got first valid mimio event on descriptor %d, res = %d\n", fd, res);
        gotevent = true;
    }
    return res;
}


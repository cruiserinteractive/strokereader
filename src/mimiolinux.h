/* $Id$ */
#ifndef MIMIOLINUX_DOT_AITCH
#define MIMIOLINUX_DOT_AITCH

/**\file mimiolinux.h
 * Declaration of Mimio Driver using linux kernel module
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#include "strokereader.h"

class SDL_Thread;
struct input_event;

/**
 * MimioLinux, pushes MimioEvents read from a device node
 * onto a dispatcher
 */
class MimioLinux {
    MimioLinux(const MimioLinux &);
    MimioLinux& operator=(const MimioLinux&);
public:
    typedef void (*DISPATCH_FUNC)(MimioEvent *ev);
    typedef int (*FALLBACK_FUNC)(const struct input_event * event, MimioLinux *ml);
    static void add_fallback(FALLBACK_FUNC f);
    static void set_fallback(FALLBACK_FUNC f);
protected:
    FALLBACK_FUNC active_fallback;
    SDL_Thread *thr;
public:
    DISPATCH_FUNC dispatch;
    int fd;

    int penid;
    int lastx;
    int lasty;
    bool gotevent;

    MimioEvent::PENSTATE penstate;

    int decode(const struct input_event * event);
    int handle_coord(const struct input_event * event);
    int handle_action(const struct input_event * event);
    void dispatch_coord();

public:
    /** Constructor */
    MimioLinux(DISPATCH_FUNC dispatcher,
               const char* device = "/dev/input/event2" );
    ~MimioLinux();
    void stop();
    int join();
    int private_run_thread();
    bool running(bool make_sure = false) {return thr && (!make_sure || gotevent);}

    /** Private struct to allow add_fallback to be called in static initialisers
     * regardless of the load order of the translation units
     */
    struct Init {static int count; Init(); ~Init();};
};

namespace {MimioLinux::Init mimiolinux_init__;}

#endif


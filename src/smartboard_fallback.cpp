/* $Id$ */
#include "mimiolinux.h"

/**\file smartboard_fallback.cpp
 * Definitions for SmartBoard fallback
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#include <linux/input.h>
#include <stdio.h>

namespace {
    struct Init{Init();};
    Init startup;

    int fallback(const struct input_event * event, MimioLinux *ml) {
        ml->penid = 1;
        if (event->type == EV_SYN && event->code == SYN_REPORT) {
            ml->dispatch_coord();
            return 1;
        }

        if (event->type == EV_ABS) {
            switch(event->code) {
            case ABS_X:
            case ABS_Y:
                ml->handle_coord(event);
                return 1;
                break;
            default:
                fprintf(stderr,
                        "[SMARTBOARD] EV_ABS Unknown axis: 0x%x (%d) value: 0x%x (%d)\n",
                        event->code, event->code, event->value, event->value);
            }
        }
        if (event->type == EV_KEY || event->type == EV_MSC) {
            if (event->code == 272 || event->code == 4) {
                ml->penstate = event->value ? MimioEvent::DOWN : MimioEvent::UP;
                return 1;
            } else {
                fprintf(stderr, "[SMARTBOARD] EV_KEY Unknown key: %d\n", event->code);
            }
        }
        fprintf(stderr,
                "[SMARTBOARD] Ignoring Event {type, code, value} = {0x%x, 0x%x, %d}\n",
                event->type, event->code, event->value);
        return 1; /* keep going for now (i.e. always indicate "valid") */
    }
}

Init::Init() {
    MimioLinux::add_fallback(&fallback);
}

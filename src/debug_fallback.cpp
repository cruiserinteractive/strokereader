/* $Id$ */
#include "mimiolinux.h"

/**\file debug_fallback.cpp
 * fallback for debugging and plumbing for inputtest program
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#include <linux/input.h>
#include <stdio.h>
#include <string>
#include <sstream>

namespace {
    struct Init{Init();};
    Init startup;

    int fallback(const struct input_event * event, MimioLinux *ml) {
        const char* type = "Unknown";
        switch (event->type) {
        case EV_SYN: type = "EV_SYN" ; break;
        case EV_KEY: type = "EV_KEY" ; break;
        case EV_REL: type = "EV_REL" ; break;
        case EV_ABS: type = "EV_ABS" ; break;
        case EV_MSC: type = "EV_MSC" ; break;
        case EV_SW: type = "EV_SW" ; break;
        case EV_LED: type = "EV_LED" ; break;
        case EV_SND: type = "EV_SND" ; break;
        case EV_REP: type = "EV_REP" ; break;
        case EV_FF: type = "EV_FF" ; break;
        case EV_PWR: type = "EV_PWR" ; break;
        case EV_FF_STATUS: type = "EV_FF_STATUS" ; break;
        case EV_MAX: type = "EV_MAX" ; break;
        }

        const char* codestr = "Unknown";
        if (event->type == EV_SYN) {
            switch (event->code) {
            case SYN_REPORT:
                codestr = "SYN_REPORT";
                ml->dispatch_coord();
                break;
            case SYN_CONFIG: codestr = "SYN_CONFIG"; break;
            }
        }
        if (event->type == EV_ABS) {
            switch(event->code) {
            case ABS_X:
                codestr = "ABS_X";
            case ABS_Y:
                codestr = "ABS_Y";
                (event->code == ABS_X ? ml->lastx : ml->lasty) = event->value;
                return 1; /* handled below, on SYN */
                break;
            case ABS_Z: codestr = "ABS_Z"; break;
            }
            ml->handle_coord(event);
        }
        if (!(event->type == EV_SYN && event->code == SYN_REPORT)) {
            if (event->type == EV_KEY) {
                if (event->code == 272) {
                    ml->penstate = event->value ? MimioEvent::DOWN : MimioEvent::UP;
                } else {
                    fprintf(stderr, "Keyboard event: %d\n", event->code);
                }
            } else {
                fprintf(stderr, "Debug callback: type = %s (0x%x)\n", type, event->type);
                fprintf(stderr, "\tCode: %s (0x%x)\n", codestr, event->code);
            }
            fprintf(stderr, "\tValue: %d\n", event->value);
            fprintf(stderr, "X: %6d Y:%6d\033[1G", ml->lastx, ml->lasty);
        } else {
            fprintf(stderr, "\033[1G\033[1A");
        }
        return 1;
    }

#ifndef NVERBOSE_DEBUG
    void dumpme(MimioEvent *e) {
        fprintf(stderr, "MimioEvent = {%s, %d, (%d, %d)}\n",
                e->state == MimioEvent::UP ?       "     UP" :
                (e->state == MimioEvent::DOWN ?    "   DOWN" :
                 (e->state == MimioEvent::MOVING ? " MOVING" :
                  "NOTHING")),
                e->user, e->x, e->y);
    }
#else
    void dumpme(MimioEvent*) {}
#endif

    void dispatch(MimioEvent *me) {
        //static bool first = true;
        dumpme(me);
        delete me;
    }

}

Init::Init() {
    MimioLinux::set_fallback(&fallback);
}

#ifdef MAINPROGRAM
int main() {
    const unsigned TOTRY=20;
    const std::string base = "/dev/input/event";
    for (unsigned t = 0; t <= TOTRY; ++t) {
        MimioLinux *ml = 0;
        std::ostringstream host;
        host << base << t;
        ml = new MimioLinux(dispatch, host.str().c_str());
        if (ml->running()) {
            fprintf(stderr, "%s running..\n", host.str().c_str());
        } else  {
            delete ml;
        }
    }
    fprintf(stderr, "Q/EOF to exit\n");
    int c;
    while ((c = getchar()) != EOF) {
        if (c == 'Q')
            break;
    }
    return 0;
}
#endif

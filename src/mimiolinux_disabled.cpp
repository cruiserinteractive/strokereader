/* $Id$ */
#include "mimiolinux.h"

/**\file mimiolinux_disabled.cpp
 * Definitions for a disabled MimioLinux class
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#include <stdio.h>
int MimioLinux::Init::count = 0;
MimioLinux::Init::Init() {}
MimioLinux::Init::~Init() {}
MimioLinux::MimioLinux(const MimioLinux &) {}
MimioLinux& MimioLinux::operator=(const MimioLinux&) {return *this;}
MimioLinux::MimioLinux(DISPATCH_FUNC, const char* )
    : thr(0)
{
    fprintf(stderr, "[MIMIOLINUX] Disabled at compile time (probably not linux!).\n");
}

void MimioLinux::stop() {}
MimioLinux::~MimioLinux() {}
int MimioLinux::join() {return 0;}
int MimioLinux::private_run_thread() {return 0;}
void MimioLinux::dispatch_coord() {}
int MimioLinux::handle_coord(const struct input_event * ) {return 0;}
int MimioLinux::handle_action(const struct input_event * ) {return 0;}
int MimioLinux::decode(const struct input_event * ) {return 0;}

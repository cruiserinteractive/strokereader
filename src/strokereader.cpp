/* $Id$ $URL$ */
#include "strokereader.h"

/** \file strokereader.cpp
 * Definitions for socket code that received MimioEvents from the MSVC/Mimio/VirtualInk
 * StrokeSender plugin.
 */

#include <string.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <deque>
#include <vector>
#include <sstream>

#include <SDL_thread.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#else
# ifdef __WIN32__
# ifndef ENABLE_NETWORK
#  define ENABLE_NETWORK
# endif
# endif
#endif

#include "packet.h"
#include "schedlock.h"
#include "rbn_solve.h"

#ifdef ENABLE_NETWORK
#include "sdl_server.h"
#endif

#include "mimiolinux.h"

#ifdef __WIN32__
#include <windows.h>
#endif

FAST_ALLOCATOR<MimioEvent> MimioEvent::alloc;

using std::deque;

static const bool DEBUG = false;

static unsigned MAX_QUEUESIZE = 7;

#define STROKE_PORT 1512

static SDL_mutex *meque_mut = SDL_CreateMutex(); //!! we don't destroy this!!
static SDL_sem *meque_sem = SDL_CreateSemaphore(0); // nor this
static SDL_sem *connect_sem = SDL_CreateSemaphore(0); // nor this
static deque<MimioEvent*> meque;
static volatile int running = 0;
static volatile bool do_terminate = false;
static int mimio_port = STROKE_PORT;
static std::string last_mimio_server;
static volatile int execute = 1;

#ifdef VERBOSE_DEBUG
static void dumpme(MimioEvent *e) {
    fprintf(stderr, "MimioEvent = {%s, %d, (%d, %d)}\n",
            e->state == MimioEvent::UP ?       "     UP" :
            (e->state == MimioEvent::DOWN ?    "   DOWN" :
             (e->state == MimioEvent::MOVING ? " MOVING" :
              "NOTHING")),
            e->user, e->x, e->y);
}
#else
static void dumpme(MimioEvent*) {}
#endif

#ifdef ENABLE_NETWORK
#ifndef NO_MIMIO 
SDL_SERVER_DEFINECLIENT_ATTR(MimioMonitor, mimio_port, STROKEREADER_API)
//static Listener listener;
#else
class MimioMonitor : Client {
protected:
    virtual int handle();
public:
    // (TCPsocket ss, Listener *ccreator);
    static Client *make(TCPsocket ss, Listener *ccreator);
    static int &port;
    static bool added;
};
namespace {
    int dummy;
}
int &MimioMonitor::port = dummy;
bool MimioMonitor::added = 0;
#endif
#endif

namespace {
    typedef std::vector<MimioLinux *> LINUX_MIMIOS;

    MimioLinux *linux_mimio = 0;
    LINUX_MIMIOS linux_mimios;

    void queue_dispatch(MimioEvent *nme) {
        if (DEBUG) fprintf(stderr, "o");
        SDL_mutexP(meque_mut);
        meque.push_back(nme);

        if (meque.size() > MAX_QUEUESIZE) {
            delete meque.front();
            meque.pop_front();
            mimio_decr();
            SDL_mutexV(meque_mut);
        } else {
            SDL_mutexV(meque_mut);
            SDL_SemPost(meque_sem);
        }
        if (DEBUG) fprintf(stderr, "Queue size is now %u", (unsigned)meque.size());
    }

    bool startLinuxMimios(MimioLinux::DISPATCH_FUNC dp, const std::string &hostmask, unsigned totry) {
        bool any = false;
        const std::string base = hostmask.substr(0, hostmask.size() -1);
        for (unsigned t = 0; t <= totry; ++t) {
            MimioLinux *ml = 0;
            std::ostringstream host;
            host << base << t;
            ml = new MimioLinux(dp, host.str().c_str());
            if (ml->running()) {
                linux_mimios.push_back(ml);
                any = true;
            } else  {
                delete ml;
            }
        }
        return any;
    }
    bool isLinuxMimio(MimioLinux::DISPATCH_FUNC dp, const char* host, bool &success, unsigned ports_to_try = 10) {
        if (*host == '/' || *host == '.') {
            if (host[strlen(host)-1] == '*') {
                success = startLinuxMimios(dp, host, ports_to_try);
            } else {
                linux_mimio = new MimioLinux(dp, host);
                if (linux_mimio->running())
                    success = true;
                else
                    delete linux_mimio;
            }
            return true;
        }
        return false;
    }
}

void strokereader_static_init() {

    linux_mimio = 0;
    linux_mimios.clear();
    meque.clear();
    running = 0;
    do_terminate = false;
    mimio_port = STROKE_PORT;
    execute = 1;

}

bool startMimioServer(const char* host, unsigned ports_to_try, int retries) {

    bool success = false;
    if (isLinuxMimio(queue_dispatch, host,success, ports_to_try))
        return success;

#ifdef ENABLE_NETWORK
    IPaddress ip;
    do {
        for (unsigned delta = 1; execute && delta <= ports_to_try; ++delta) {
            if (SDLNet_ResolveHost(&ip, host, STROKE_PORT + delta) == 0) {
                fprintf(stderr, "Trying to signal StrokeSender on %s:%d... ", host, STROKE_PORT+delta);
                TCPsocket sig = SDLNet_TCP_Open(&ip);
                if (sig) {
                    SDLNet_TCP_Close(sig);
                    fprintf(stderr, "Signalled StrokeSender OK\n");
                    //last_mimio_server = host;
                    return true;
                } else {
                    fprintf(stderr, "StrokeSender signal fail (%s)\n", SDL_GetError());
                }
            }
        }
        fprintf(stderr, "ERROR: Unable to signal StrokeSender!\n");
    } while (retries--);
#endif
    return success;
}

void waitForMimioConnect() {
    if (linux_mimio || linux_mimios.size())
        return;
    while (!do_terminate && SDL_SemWait(connect_sem) != 0 && !do_terminate)
        ;
}

MimioEvent *pollRecentMimio() {
    MimioEvent *ret = 0;
    SDL_mutexP(meque_mut);

    //clear the queue and leave the last one in ret
    while (meque.size() > 0) {
        while (SDL_SemWait(meque_sem) != 0)
            ; //should never block
        if (ret) {
            mimio_decr();
            delete ret;
        }
        ret = meque.front();
        meque.pop_front();
    }

    SDL_mutexV(meque_mut);
    return ret;
}

MimioEvent *pollOldMimio() {
    MimioEvent *ret = 0;
    SDL_mutexP(meque_mut);
    if (meque.size() > 0) {
        while (SDL_SemWait(meque_sem) != 0)
            ; //should never block
        ret = meque.front();
        meque.pop_front(); //rely on client to decr_mimio()
    }
    SDL_mutexV(meque_mut);
    return ret;
}

MimioEvent *waitRecentMimio() {
    MimioEvent *ret = 0;
    if (DEBUG) fprintf(stderr, "Waiting for meque sem post in recent\n");
    while (SDL_SemWait(meque_sem) != 0)
        ;
    SDL_mutexP(meque_mut);

    if (meque.empty()) {
        fprintf(stderr, "CRITICAL -- queue is empty but got post (maybe we are shutting down) in %s at %s:%d\n",
                __func__, __FILE__, __LINE__);
        SDL_mutexV(meque_mut);
        return 0;
    }

    ret = meque.back();
    meque.pop_back();

    //delete anything older that what we're about to return
    while (meque.size() > 0) {
        while (SDL_SemWait(meque_sem) != 0)
            ;
        delete meque.back();
        mimio_decr();
        meque.pop_back();
    }

    SDL_mutexV(meque_mut);
    return ret;
}

MimioEvent *waitOldMimio() {
    MimioEvent *ret = 0;
    if (DEBUG) fprintf(stderr, "Waiting for meque sem post in old\n");
    while (SDL_SemWait(meque_sem) != 0)
        ;
    SDL_mutexP(meque_mut);
    ret = meque.front();
    meque.pop_front();
    SDL_mutexV(meque_mut);
    return ret;
}


void cleanupMimioReader() {
     //disconnect all servers
     //wait for threads
    fprintf(stderr, "Cleaning up Mimio Reader...\n");
    SDL_mutexP(meque_mut);
    do_terminate = true;
    SDL_SemPost(connect_sem);
    while (!meque.empty()) {
        while (SDL_SemWait(meque_sem) != 0)
            ;
        delete meque.front();
        mimio_decr();
        meque.pop_front();
    }
    meque.push_front(0);
    SDL_mutexV(meque_mut);
    SDL_SemPost(meque_sem);
}

bool isMimioConnected() {
    for (LINUX_MIMIOS::iterator it = linux_mimios.begin(); it != linux_mimios.end(); ++it)
        if ((*it)->running(true)) //make sure
            return true;
    return running || (linux_mimio && linux_mimio->running()); //client && client->isConnected();
}

namespace {
    XYInterp *interpolator;
    bool hide_cursor = true;

    SDL_Thread *thr;

    unsigned int *num_events_p = 0;
    unsigned int max_events;

    AUTOCALIBRATE_CALLBACK event_callback;
    HIDE_CURSOR_CALLBACK hide_cursor_cb;

    const int MIMIO_WIDTH = 9600;
    const int MIMIO_HEIGHT = 4800;

    unsigned scr_width;
    unsigned scr_height;

    void dispatch(MimioEvent *me) {
        if (DEBUG) fprintf(stderr, "x");
        static bool first = true;
        dumpme(me);
#if 0
        if  (me->state == MimioEvent::MOVING &&
             num_events_p  &&
             *num_events_p  > max_events) {
            delete me;
            mimio_decr();
            return;
        }
#endif
        if (hide_cursor_cb && first) {
            //pushUserEvent(UE_HIDE_CURSOR);
            hide_cursor_cb();
            first = false;
        }

        /* calibrate me->(x, y) */
        XYInterp::INVEC in;
        in[0] = 1.0*me->x/MIMIO_WIDTH;
        in[1] = 1.0*me->y/MIMIO_HEIGHT;
        XYInterp::OUTVEC out;
        interpolator->sub(out, in);
        me->x = static_cast<int>(scr_width *out[0] + 0.5);
        me->y = static_cast<int>(scr_height*out[1] + 0.5);
	
	//fprintf(stderr, "[] Event callback of %p\n", reinterpret_cast<void*>(event_callback));
        event_callback(me);
        delete me;
        //We CAN'T rely on event queue to mimio_decr()
        //because that's what will be starving!!!
        mimio_decr();
    }

    int callback_loop(void*) {
        MimioEvent *me;

        startMimioServer(last_mimio_server.c_str());
        fprintf(stderr, "Waiting for mimio connection from %s...\n", last_mimio_server.c_str());

#ifdef __WIN32__
        ::SetThreadPriority(::GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
#endif
        waitForMimioConnect();
        fprintf(stderr, "Mimio Connected.\n");
        while (execute && (me = waitOldMimio()) && execute) {
            dispatch(me);
        }
        return 0;

    }
}

bool startAutoCalibrator(AUTOCALIBRATE_CALLBACK callback,
                         unsigned screen_width,
                         unsigned screen_height,
                         const char* host_or_path /*= "127.0.0.1"*/,
                         HIDE_CURSOR_CALLBACK hide_cursor /*= 0*/,
                         unsigned int* event_number_tracker /*= 0*/,
                         unsigned int  max_event_backlog /*= 1*/,
                         const char* network_dat /*= "network.dat"*/) {
    event_callback = callback;
    scr_width = screen_width;
    scr_height = screen_height;
    hide_cursor_cb = hide_cursor;
    num_events_p = event_number_tracker;
    max_events = max_event_backlog;
    interpolator = new XYInterp(network_dat);

    if (!interpolator->ready()) {
        fprintf(stderr, "Couldn't load RBF Network from %s -- you might need to callibrate!\n", network_dat);
        return false;
    }

    //    waitForMimioConnect();
    //    fprintf(stderr, "Connected.d\n");
    //getchar();
    last_mimio_server = host_or_path;
    bool success = false;
    if (isLinuxMimio(dispatch, host_or_path, success)) {
        return success;
    } else {
        thr = SDL_CreateThread(callback_loop, 0);
    }
    return thr;
}

void cleanupAutoCalibrator() {
    execute = 0;
    for (LINUX_MIMIOS::iterator it = linux_mimios.begin(); it != linux_mimios.end(); ++it) {
        (*it)->stop();
        fprintf(stderr, "Waiting for MimioLinux thread #N\n");
        (*it)->join();
    }
    if (linux_mimio) {
        linux_mimio->stop();
        fprintf(stderr, "Waiting for MimioLinux thread\n");
        linux_mimio->join();
    } else {
        cleanupMimioReader();
        fprintf(stderr, "Waiting for Mimio thread\n");
        // Don't do this while we probably don't support it anyway
        // has a habit of locking
        //SDL_WaitThread(thr, 0);
    }
    delete interpolator;
}

#ifdef ENABLE_NETWORK

int MimioMonitor::handle() {
    MimioPacket mp;
    int pad;
    int result = 0;
#ifdef WIN32
    ::SetThreadPriority(::GetCurrentThread(), THREAD_PRIORITY_HIGHEST); //11
#endif
    pad = 0;
    SDL_SemPost(connect_sem);
    running++;
    fprintf(stderr, "First block for read\n");
    while (!do_terminate && run && (result = SDLNet_TCP_Recv(s, (void*)&mp, sizeof(mp))) > 0) {
	if (do_terminate)
	    break;
        mp.user = SDLNet_Read32((void*)&mp.user);
        mp.action = SDLNet_Read16((void*)&mp.action);
        mp.x = SDLNet_Read16((void*)&mp.x);
        mp.y = SDLNet_Read16((void*)&mp.y);
	mimio_incr(); //we have an event
        if (DEBUG)
            fprintf(stderr, "Received, %hd for %ld at (%hd, %hd)\n", mp.action, mp.user, mp.x, mp.y);

        MimioEvent *nme = new MimioEvent(mp.action == MimioPacket::MOVE ? MimioEvent::MOVING :
                                         (mp.action == MimioPacket::UP ? MimioEvent::UP :
                                          (mp.action == MimioPacket::DOWN ? MimioEvent::DOWN :
                                           MimioEvent::NOTHING)), mp.x, mp.y, mp.user);
        queue_dispatch(nme);

    }
    running--;
    SDL_ShowCursor(1);
    meque.erase(meque.begin(), meque.end());
    fprintf(stderr, "Leaving monitorThread (result = %d, do_terminate = %d)\n", result, (int)do_terminate);
    if (!do_terminate && run) {
        fprintf(stderr, "Trying to restart mimio server at intervals of 5 seconds\n");
        while (!startMimioServer(last_mimio_server.c_str()))
            SDL_Delay(5000);
        fprintf(stderr, "Managed to re-signal mimio server!\n");
    }
    return 0;
}

#endif

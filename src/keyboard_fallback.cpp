/* $Id$ */
#include "mimiolinux.h"

/**\file keyboard_fallback.cpp
 * Definitions for Keyboard fallback
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#include <linux/input.h>
#include <stdio.h>

namespace {
    struct Init{Init();};
    Init startup;

    int fallback(const struct input_event * event, MimioLinux * /*ml*/) {
        if (event->type == EV_KEY && event->code < KEY_UNKNOWN) {
            fprintf(stderr, "Keyboard event: %d\n", event->code);
            return 1;
        }
        return 0;
    }
}

Init::Init() {
    MimioLinux::add_fallback(&fallback);
}

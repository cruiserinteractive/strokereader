/* $Id$ $URL$ */
#include "schedlock.h"

/**\file template.cpp
 * Definitions for schedlock.h
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#include "SDL.h"
#include "SDL_thread.h"

#ifdef EFENCE_DEBUG
#include "efencepp.h"
static int init_ = fprintf(stderr, "%s init begin\n", __FILE__);
#endif

#ifdef _WIN32

namespace {
    SDL_sem *mimio_sem = SDL_CreateSemaphore(0);
    SDL_mutex *mimio_mut = SDL_CreateMutex();
    unsigned num_events = 0;
    bool waiting = false;
    unsigned starve_count = 0;
    const unsigned MAX_STARVE_COUNT = 30;
}

void starve_for_mimio() {
    bool we_will_wait;
    SDL_mutexP(mimio_mut);
    we_will_wait = num_events > 1;
    waiting = we_will_wait;
    SDL_mutexV(mimio_mut);
    if (we_will_wait)
        SDL_SemWait(mimio_sem);
}

void mimio_incr() {
    SDL_mutexP(mimio_mut);
    ++num_events;
    SDL_mutexV(mimio_mut);
}

void mimio_decr() {
    SDL_mutexP(mimio_mut);
    --num_events;
    if (waiting) {
        if (num_events == 0 || starve_count > MAX_STARVE_COUNT) {
            starve_count = 0;
            waiting = false;
            SDL_SemPost(mimio_sem);
        } else {
            ++starve_count;
        }
    }
    SDL_mutexV(mimio_mut);
}

int get_num_mimio_events() {
	return num_events;
}
#endif


/* $Id$ */
#ifndef MIMIODEFINES_DOT_AITCH
#define MIMIODEFINES_DOT_AITCH

/**\file mimiodefines.h
 * Defines for the Mimio Driver using linux kernel module
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Rev$
 * $Date$
 */

#define MIMIO_EV_SYNC	(EV_SYN)
#define MIMIO_EV_ACTION	(EV_KEY)
#define MIMIO_EV_COORD	(EV_ABS)

#define MIMIO_AXIS_X	(ABS_X)
#define MIMIO_AXIS_Y	(ABS_Y)

#define MIMIO_INSTR_MIN		(BTN_TOOL_PEN)
#define MIMIO_PEN_BLACK		(MIMIO_INSTR_MIN + 0)
#define MIMIO_PEN_BLUE		(MIMIO_INSTR_MIN + 1)
#define MIMIO_PEN_GREEN		(MIMIO_INSTR_MIN + 2)
#define MIMIO_PEN_RED		(MIMIO_INSTR_MIN + 3)
#define MIMIO_BIG_ERASER	(MIMIO_INSTR_MIN + 4)
#define MIMIO_LIL_ERASER	(MIMIO_INSTR_MIN + 5)
/* mimio interactive */
#define MIMIO_PEN_INTERACTIVE	(MIMIO_INSTR_MIN + 6)
#define MIMIO_INTERACTIVE_B1	(MIMIO_INSTR_MIN + 7)
#define MIMIO_INTERACTIVE_B2	(MIMIO_INSTR_MIN + 8)
#define MIMIO_EXTRA_PENS	(MIMIO_INSTR_MIN + 9)

#define MIMIO_INSTR_MAX		(MIMIO_EXTRA_PENS)

#define MIMIO_BTN_MIN			(BTN_MISC)
#define MIMIO_BTN_MEMRESET		(MIMIO_BTN_MIN + 0)
#define MIMIO_BTN_NEWPAGE		(MIMIO_BTN_MIN + 1)
#define MIMIO_BTN_TAGPAGE		(MIMIO_BTN_MIN + 2)
#define MIMIO_BTN_PRINTPAGE		(MIMIO_BTN_MIN + 3)
#define MIMIO_BTN_MAXIMIZE		(MIMIO_BTN_MIN + 4)
#define MIMIO_BTN_FINDCTLPNL	(MIMIO_BTN_MIN + 5)
#define MIMIO_BTN_MAX			(MIMIO_BTN_FINDCTLPNL)

#define IsValidBtn(x) ((x) >= MIMIO_BTN_MIN && (x) <= MIMIO_BTN_MAX)
#define IsValidInstr(x) ((x) >= MIMIO_INSTR_MIN && (x) <= MIMIO_INSTR_MAX)

static const char * const mimio_instrs[] = {
    "black pen",
    "blue pen",
    "green pen",
    "red pen",
    "big eraser",
    "lil eraser",
    "mimio interactive",
    "interactive btn1",
    "interactive btn2",
    "extra pens"
};

static const char * const mimio_btns[] = {
    "mem-reset",
    "new page",
    "tag page",
    "print page",
    "maximize",
    "find control panel"
};

#endif

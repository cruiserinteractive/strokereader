
#include "client_vision.h"
#include <string.h>


void init_vision(){
	/* pthread_t un_p;
  int argument = 0;
  int *resultat;
  fprintf(stderr, "Starting vision tracker.....................");
  int erreur = 0;//pthread_create (&un_p, NULL, start_vision, &argument);
  if (erreur != 0){
	  fprintf(stderr,"....................Echec : %d\n",erreur);
  }else {
	fprintf(stderr, ".....................done\n");

  }
  pthread_join (un_p, (void **)&resultat); */
}

void *start_vision (void *arg){

	int			sockfd;
	struct sockaddr_in	cli_addr, serv_addr;

	//pname = argv[0];

	/*
	 * Fill in the structure serv_addr with the address of the
	 * server that we want to send to.
	 */

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family		= AF_INET;
	serv_addr.sin_addr.s_addr	= inet_addr(SERV_HOST_ADDR);
	serv_addr.sin_port		= htons(SERV_UDP_PORT);

	/*
	 * Open a UDP socket (an Internet datagram socket).
	 */


	if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		fprintf(stderr,"client: can't open datagram socket");

	/*
	 * Bind any local address for us
	 */
	fprintf(stderr,"Connecting to tracker vision server ................");
	bzero((char *) &cli_addr, sizeof(cli_addr));	/* zero out */
	cli_addr.sin_family		= AF_INET;
	cli_addr.sin_addr.s_addr	= htonl(INADDR_ANY);
	cli_addr.sin_port		= htons(0);
	if (bind(sockfd, (struct sockaddr *) &cli_addr, sizeof(cli_addr)) < 0)
		fprintf(stderr,"client:can't bind local address");
	fprintf(stderr,"................done\n");


	int ok = 1;
	char recvline[1501];
	int	n, fromlen;
	struct sockaddr_in from;
	while (ok){
		fprintf(stderr,"Attente de reception		................");
		/*
		 * Now read a message from the socket and write it to
		 * our standard output.
		 */

		 fromlen = sizeof(from);
		 n = 0; //recvfrom(sockfd, recvline, 1500,0, (struct sockaddr *)&from , &fromlen);
		if (n < 0)
			fprintf(stderr,"dg_cli: recvfrom error");
		recvline[n] = 0;	/* null terminate */
		fputs(recvline, stdout);
	}
	fprintf(stderr,"fermeture de la socket");
//	close(sockfd);
	return 0;
}

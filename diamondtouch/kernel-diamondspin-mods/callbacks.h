#ifndef _CALLBACKS_H
#define _CALLBACKS_H


#include "input.h"


typedef struct segment_dt {
	int begin;
	int end;
} segment_dt;

typedef struct point_dt {
	int x ;
	int y ;
	double vx ;
	double vy ;
	int ite;
	int id;
} point_dt;

void find_fingers(struct input_set *is);
void find_segment(unsigned char *rc, int num, int threshold, list_t *list_seg);
void free_seg(list_t * list,lnode_t * node, void * ctxt);

#endif /* _CALLBACKS_H */

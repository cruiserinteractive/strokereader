#include <math.h>
#include <stdlib.h>
#include "callbacks.h"

//#include <stdio.h>


/* pixmap sizes for each piece of the display */


int cols[11], rows[11];
list_t *all_possibilities = NULL;
list_t *mapping_cr_all_pos;
int global_ID = 0;



// "is" is the input set for the associated user
void find_fingers(struct input_set *is) {

  lnode_t *node= NULL, *node2= NULL, *node3= NULL,  *best_new_node=NULL, *best_old_node=NULL, *insertion_node= NULL, *nil= NULL,*nil2= NULL;
  point_dt *point = NULL;
  int delta_t, max;
  int i,j,nb_doigt = 0, m;
  double min = INT_MAX, min_x = INT_MAX, min_y = INT_MAX,dist, dist_x, dist_y, posx, posy;


	// si j'ai au moins un doigt de detecté en x et en y
		if (list_count(is->col_seg) !=0 &&  list_count(is->row_seg) != 0){

			// calcul du maximun pour chaque segment_dt // en meme temps calcul de la valeur du segment
			// A remplacer par une mixure de gauxienne
			node = list_first(is->col_seg); // gestion des col
			nil = &(is->col_seg)->list_nilnode;
			j = 0;
			while (node != nil) {
				max = 0;
				for (i = ((segment_dt *)node->list_data)->begin; i <= ((segment_dt *)node->list_data)->end ; i++) {
					if (is->cols[i] > max) {
					  max = is->cols[i];
					  cols[j] = i;
					}
				}
				node = node->list_next;
				j++;
			}
			node = list_first(is->row_seg); // gestion des lignes
			nil = &(is->row_seg)->list_nilnode;
			j = 0;
			while (node != nil) {
				max = 0;
				for (i = ((segment_dt *)node->list_data)->begin; i <= ((segment_dt *)node->list_data)->end ; i++) {
					if (is->rows[i] > max) {
					  max = is->rows[i];
					  rows[j] = i;
					}
				}
				node = node->list_next;
				j++;
			}
			// le nombre de doigts est supérieur ou egale au max des 2 des segments en x ou en y
			// ici on concidere qu'il est egal
			if (is->col_seg->list_nodecount >= is->row_seg->list_nodecount){
				nb_doigt =  is->col_seg->list_nodecount;
			}else{
				nb_doigt =  is->row_seg->list_nodecount;
			}
			// mise au format d'affichage
			for (j = 0; j < nb_doigt; j++) {
				if (!dt.columns_left_to_right) {
					cols[j] =  dt.num_columns - cols[j];
				}
				if (!dt.rows_top_to_bottom) {
					rows[j] = dt.num_rows - rows[j];
				}
			}
			if (all_possibilities==NULL){
				all_possibilities = list_create(LISTCOUNT_T_MAX);
			}else{
				list_process(all_possibilities,NULL, &free_seg);
				list_destroy_nodes(all_possibilities);
			}
			//calcul de toutes les possibilités
			for (i = 0; i<list_count(is->col_seg); i++){
				for (j = 0; j<list_count(is->row_seg); j++){// la posibilité de point nb est constituée de la col i et la ligne j
					point = malloc (sizeof(point_dt));
					point->x = cols[i];
					point->y = rows[j];
					point->vx = 0;
					point->vy = 0;
					list_append(all_possibilities, lnode_create(point));
					//mapping_cr_all_pos[nb].x = i;
					//mapping_cr_all_pos[nb].y = j;
				}
			}
			// calcul des positions des doigts
			// le nombre de doigts n'a pas changé ... ils ont juste bougé
			if (nb_doigt == is->nb_finger){
			   // on cherche le minimum de variation pour chaque doigt
				node3 = ((lnode_t *)&(is->list_finger)->list_nilnode)->list_next;
				nil2 = &(is->list_finger)->list_nilnode;
				insertion_node = nil2;
				while (node3 != nil2) {  // pour tous mes doigts
					min = INT_MAX;
					node2 = node3;
					while (node2 != nil2) {  // je cherche la distance min parmis tous les doigts non traités
						node = ((lnode_t *)&(all_possibilities)->list_nilnode)->list_next; //list_first(all_possibilities);
						nil = &(all_possibilities)->list_nilnode;
						posx = ((point_dt *)node2->list_data)->x + (((point_dt *)node2->list_data)->vx*(is->iterations - ((point_dt *)node2->list_data)->ite));
						posy = ((point_dt *)node2->list_data)->y + (((point_dt *)node2->list_data)->vy*(is->iterations - ((point_dt *)node2->list_data)->ite));
						while (node != nil) {
							dist_x = (((point_dt *)node->list_data)->x-posx) * (((point_dt *)node->list_data)->x-posx);
							dist_y = (((point_dt *)node->list_data)->y-posy) * (((point_dt *)node->list_data)->y-posy);
							dist = dist_x + dist_y ;//sqrt (dist_x+dist_y);
							if (dist <= min) {
							  min = dist;
							  min_x = dist_x;
							  min_y = dist_y;
							  best_new_node = node; //  nouveau doigt le plus prometteur
							  best_old_node = node2;//  l'ancien doigt correspondant au doigt le plus prometteur
							}
							node = node->list_next;
						}
						node2 = node2->list_next;
					}

					if (((min_x <= 1) && (min_y >= 144)) || ((min_y <= 1) && (min_x >= 144))){// découverte d'un doigt en phase de disparition
						best_old_node = list_delete (is->list_finger, best_old_node); // suppression de l'ancien doigt de la liste des doigts
						nb_doigt--;
						is->nb_finger = nb_doigt;
						node3 = insertion_node->list_next;
				//		fprintf(stderr, "Point annulé\n");
					}else {
                  //      fprintf(stderr, "== > Point NON annulé : ENTREE\n");
						best_old_node = list_delete (is->list_finger, best_old_node); // suppression de l'ancien doigt de la liste des doigts
						best_new_node = list_delete (all_possibilities, best_new_node); // suppression du nouveau doigt de la liste des possibilité
						list_ins_after(is->list_finger, best_new_node, insertion_node); //insertion du nouveau doigtdans la liste des doigts
						insertion_node = best_new_node; //mise a jour du point d'insertion dans la liste des doigts
						node3 = insertion_node->list_next;


						delta_t = is->iterations - ((point_dt *)best_old_node->list_data)->ite;
						((point_dt *)insertion_node->list_data)->vx = (((point_dt *)insertion_node->list_data)->x - ((point_dt *)best_old_node->list_data)->x)/delta_t;
						((point_dt *)insertion_node->list_data)->vy = (((point_dt *)insertion_node->list_data)->y - ((point_dt *)best_old_node->list_data)->y)/delta_t;

						((point_dt *)insertion_node->list_data)->id = ((point_dt *)best_old_node->list_data)->id;
						((point_dt *)insertion_node->list_data)->ite = is->iterations;
						//is->nb_finger = nb_doigt ; // inutile
					}
				}


			// disparition d'un doigt (ou plus)
			}else if (nb_doigt < is->nb_finger){
			    node3 = ((lnode_t *)&(is->list_finger)->list_nilnode)->list_next;
				nil2 = &(is->list_finger)->list_nilnode;
				insertion_node = nil2;
				m = 0;
				while (node3 != nil2) {  // pour tous mes doigts
					min = 99999;
					if (m < nb_doigt){
						m++;
						node2 = node3;
						while (node2 != nil2) {  // je cherche la distance min parmis tous les doigts non traités
							node = ((lnode_t *)&(all_possibilities)->list_nilnode)->list_next; //list_first(all_possibilities);
							nil = &(all_possibilities)->list_nilnode;
							posx = ((point_dt *)node2->list_data)->x + (((point_dt *)node2->list_data)->vx*(is->iterations - ((point_dt *)node2->list_data)->ite));
							posy = ((point_dt *)node2->list_data)->y + (((point_dt *)node2->list_data)->vy*(is->iterations - ((point_dt *)node2->list_data)->ite));
							while (node != nil) {
								dist = sqrt (pow (((point_dt *)node->list_data)->x-posx,2)+pow (((point_dt *)node->list_data)->y-posy,2));
								if (dist <= min) {
								  min = dist;
								  best_new_node = node; //  nouveau doigt le plus prometteur
								  best_old_node = node2;//  l'ancien doigt correspondant au doigt le plus prometteur
								}
								node = node->list_next;
							}
							node2 = node2->list_next;
						}
						best_old_node = list_delete (is->list_finger, best_old_node); // suppression de l'ancien doigt de la liste des doigts
						best_new_node = list_delete (all_possibilities, best_new_node); // suppression du nouveau doigt de la liste des possibilité
						list_ins_after(is->list_finger, best_new_node, insertion_node); //insertion du nouveau doigtdans la liste des doigts
						insertion_node = best_new_node; //mise a jour du point d'insertion dans la liste des doigts
						node3 = insertion_node->list_next;


						delta_t = is->iterations - ((point_dt *)best_old_node->list_data)->ite;
						((point_dt *)insertion_node->list_data)->vx = (((point_dt *)insertion_node->list_data)->x - ((point_dt *)best_old_node->list_data)->x)/delta_t;
						((point_dt *)insertion_node->list_data)->vy = (((point_dt *)insertion_node->list_data)->y - ((point_dt *)best_old_node->list_data)->y)/delta_t;

						((point_dt *)insertion_node->list_data)->id = ((point_dt *)best_old_node->list_data)->id;
						((point_dt *)insertion_node->list_data)->ite = is->iterations;
					}else{
						//elimination des doigts qu'il reste dans la liste
						node3 =	node3->list_next;
						free (list_delete(is->list_finger, node3->list_prev)->list_data);
					}
				}
				is->nb_finger = nb_doigt;

			}else { // apparition d'un doigt
					// on cherche le minimum de variation pour chaque doigt
				node3 = ((lnode_t *)&(is->list_finger)->list_nilnode)->list_next; //list_first(is->list_finger);
				nil2 = &(is->list_finger)->list_nilnode;
				insertion_node = nil2;
				while (node3 != nil2) {  // pour tous mes doigts
					min = 99999;
					node2 = node3;
					while (node2 != nil2) {  // je cherche la distance min parmis tous les doigts non traités
						node = ((lnode_t *)&(all_possibilities)->list_nilnode)->list_next; //list_first(all_possibilities);
						nil = &(all_possibilities)->list_nilnode;
						posx = ((point_dt *)node2->list_data)->x + (((point_dt *)node2->list_data)->vx*(is->iterations - ((point_dt *)node2->list_data)->ite));
						posy = ((point_dt *)node2->list_data)->y + (((point_dt *)node2->list_data)->vy*(is->iterations - ((point_dt *)node2->list_data)->ite));
						while (node != nil) {
							dist = sqrt (pow (((point_dt *)node->list_data)->x-posx,2)+pow (((point_dt *)node->list_data)->y-posy,2));
							if (dist <= min) {
							  min = dist;
							  best_new_node = node; //  nouveau doigt le plus prometteur
							  best_old_node = node2;//  l'ancien doigt correspondant au doigt le plus prometteur
							}
							node = node->list_next;
						}
						node2 = node2->list_next;
					}
					best_old_node = list_delete (is->list_finger, best_old_node); // suppression de l'ancien doigt de la liste des doigts
					best_new_node = list_delete (all_possibilities, best_new_node); // suppression du nouveau doigt de la liste des possibilité
					list_ins_after(is->list_finger, best_new_node, insertion_node); //insertion du nouveau doigtdans la liste des doigts
					insertion_node = best_new_node; //mise a jour du point d'insertion dans la liste des doigts
					node3 = insertion_node->list_next;

					delta_t = is->iterations - ((point_dt *)best_old_node->list_data)->ite;
					((point_dt *)insertion_node->list_data)->vx = (((point_dt *)insertion_node->list_data)->x - ((point_dt *)best_old_node->list_data)->x)/delta_t;
					((point_dt *)insertion_node->list_data)->vy = (((point_dt *)insertion_node->list_data)->y - ((point_dt *)best_old_node->list_data)->y)/delta_t;

					((point_dt *)insertion_node->list_data)->id = ((point_dt *)best_old_node->list_data)->id;
					((point_dt *)insertion_node->list_data)->ite = is->iterations;

					//Dans la liste des possibilité mise à 1 de tous les vx/vy des points ayant le meme x/y que le point choisi
					if (insertion_node!=nil2){
						node = ((lnode_t *)&(all_possibilities)->list_nilnode)->list_next;
						nil = &(all_possibilities)->list_nilnode;
						while (node != nil) {
							if (((point_dt *)node->list_data)->x==((point_dt *)insertion_node->list_data)->x){
								((point_dt *)node->list_data)->vx = 1;
							}
							if (((point_dt *)node->list_data)->y==((point_dt *)insertion_node->list_data)->y){
								((point_dt *)node->list_data)->vy = 1;
							}
							node = node->list_next;
						}
					}
				}
				// TODO prendre en compte l'apparition en face d'un doigt existant
				if (list_count(is->col_seg) == list_count(is->row_seg)) { // meme nombre de lecture en x est en y
					node = ((lnode_t *)&(all_possibilities)->list_nilnode)->list_next;
					nil = &(all_possibilities)->list_nilnode;
					while (node != nil) {
						if (((point_dt *)node->list_data)->vx!=1 && ((point_dt *)node->list_data)->vy!=1){
							point = malloc (sizeof(point_dt));
							point->x = ((point_dt *)node->list_data)->x;
							point->y = ((point_dt *)node->list_data)->y;
							point->vx = 0;
							point->vy = 0;
							point->ite = is->iterations;
							global_ID++;
							point->id = global_ID;
							list_append(is->list_finger, lnode_create(point));
							is->nb_finger = is->nb_finger+1 ;

						}
						node = node->list_next;
					}
				}
			}
		}else if (list_count(is->col_seg) ==0 &&  list_count(is->row_seg) == 0){
			list_process(is->list_finger,NULL, &free_seg);
			list_destroy_nodes(is->list_finger);
			is->nb_finger = 0;
		}

}

void free_seg(list_t * list,lnode_t * node, void * ctxt){
	free (node->list_data);
}

void find_segment(unsigned char *rc, int num, int threshold, list_t *list_seg){

	int i;
	boolean in_segment = FALSE;
	boolean down = FALSE;
	int last_value = 0;
	segment_dt * seg = NULL;
	if (list_count(list_seg)!= 0){
		list_process(list_seg,NULL, &free_seg);
		list_destroy_nodes(list_seg);
	}
	for (i = 0; i < num; i++) {
		if (rc[i]<threshold){  // inférieur au seuil
			if (in_segment){
				in_segment = FALSE;
			}
		}else {					// suppérieur au seuil
			if (in_segment){  // deja dans le segment
				if (last_value < rc[i]){
					if (down) { // nouveau doigt
						seg = malloc (sizeof(segment_dt));
						seg->begin = i;
						seg->end = i;
						list_append(list_seg, lnode_create(seg));
						down = FALSE;
					}else {
						// mise a jour de la veleur de fin du segment
						segment_dt *seg =  (list_last(list_seg))->list_data;
						seg->end = i;
						down = FALSE;
					}
				}else { // j'ai passé la valeur maximale du segment
					// mise a jour de la veleur de fin du segment
					segment_dt *seg =  (list_last(list_seg))->list_data;
					seg->end = i;
					down = TRUE;
				}
			}else{
				seg = malloc (sizeof(segment_dt));
				seg->begin = i;
				seg->end = i;
				list_append(list_seg, lnode_create(seg));
				in_segment = TRUE;
				down = FALSE;
			}
		}
		last_value = rc[i];
	}
}


#include "input.h"
#include "client_vision.h"
#include "gui.h"
#include "dt_demo.h"
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <fcntl.h>


void usage() {
	fprintf(stderr, "Usage: dt_test <device>\n");
}

int main(int argc, char *argv[]) {
    int fd;
    char *devname = NULL;
 	switch (argc) {
  		case 1: devname = DEV_FNAME; break;
  		case 2: devname = argv[1]; break;
  		default: usage(); exit(-1); break;
  	}

    fd = init_input(devname);

    gdk_input_add (fd, GDK_INPUT_READ, (void *)reader_func, (void *) (is));

    init_vision();
    init_display(argc, argv);

    return 0;
}

void reader_func(void * data, int source, GdkInputCondition condition) {
  int ret, i;
  struct input_set *is = (struct input_set *) data;

  ret = read_data(source);
  if (ret < 0) {
    if (errno == EAGAIN) {
      fprintf(stderr, "reader func would block -- ??\n");
      return;
    }
    else {
      gtk_main_quit();
      return;
    }
  }
  if (ret != dt.read_size) {
    fprintf(stderr, "read returned %d instead of %d\n", ret, dt.read_size);
  }

  for (i = 0; i < dt.num_users; i++) {
    process_data(is + i);
    display_data(is + i);
  }
  recycle_display();
}


#include "../input.h"
#include "../fingers_detection.h"
#include "diamondtouch.h"

#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>


int col_threshold = COL_THRESHOLD;
int row_threshold = ROW_THRESHOLD;
int res_scale = 16;
int detection_mode = MODE_2_FINGERS_ONLY;

unsigned char eebuff[0x40];
struct dt_eeparse dt;
char *dt_device_name;
unsigned char *buff;


unsigned *seq;
int seq_misses;
unsigned last_seq;
int largest_seq_diff;
int shield_drv_on;


struct input_set is[MAX_INPUTS];


int init(int mode){
    int fd;
    detection_mode = mode;
    char *devname =  DEV_FNAME;
    fd = init_input(devname);
    return fd;
}

/**
 * on linux it is not the native lib that start the thread pumping the event.
 */
int start_plateform() {
  return 0;
}

int init_input(char *devname){
  int ret, i, fd, rowscols;

  dt_device_name = devname;

  fd = open(devname, O_RDONLY | O_NONBLOCK);
  if (fd < 0) {
    fprintf(stderr, "Could not open device %s\n", devname);
  }

  ret = ioctl(fd, DT_IOCGPARAMS, eebuff);
  if (ret < 0) {
    perror("ioctl");
    fprintf(stderr, "ioctl failed with ret = %d\n", ret);
  }

  dt_parse_params(eebuff, &dt);

  rowscols = dt.num_rows + dt.num_columns;
  if (dt.shield_drv_on) rowscols++;

  buff = (unsigned char *) malloc(dt.read_size);
  if (buff == NULL) {
    fprintf(stderr, "Could not allocate read buffer.\n");
  }


  largest_seq_diff = -1;
  seq_misses = -1;
  last_seq = 0;
  seq = (unsigned *) buff; // point to the sequence number at the beginning

  for (i = 0; i < dt.num_users; i++) {
    is[i].iterations = 0;
    is[i].number = i;
	is[i].row_seg = list_create(dt.num_rows);
	is[i].col_seg = list_create(dt.num_columns);
	is[i].list_finger = list_create(LISTCOUNT_T_MAX);
	is[i].nb_finger = 0;
	is[i].state = NO_TOUCH_0_TO_0;
    if (dt.rows_first) {
      is[i].rows = buff + 4 + i * rowscols;
      is[i].cols = is[i].rows + dt.num_rows;
    }
    else {
      is[i].cols = buff + 4 + i * rowscols;
      is[i].rows = is[i].cols + dt.num_columns;
    }
  }
  return fd;
}

int read_data(int source){
    int i =0;
    int ret = read(source, buff, dt.read_size);
    for (i = 0; i<4; i++){
        process_data(is+i);
    }
    return ret;
}

/* calculations to perform on the input data */
void process_data(struct input_set *isi) {
    int i=0, tmp=0;
    if (!dt.rows_top_to_bottom){
        for(i=0; i<((int)(dt.num_rows/2));i++){
            tmp = isi->rows[i];
            isi->rows[i] = isi->rows[dt.num_rows-i-1];
            isi->rows[dt.num_rows-i-1] = tmp;
        }
    }
    find_segment(isi->rows, dt.num_rows, row_threshold, isi->row_seg);
    if (!dt.columns_left_to_right){
        for(i=0; i<((int)(dt.num_columns/2));i++){
            tmp = isi->cols[i];
            isi->cols[i] = isi->cols[dt.num_columns-i-1];
            isi->cols[dt.num_columns-i-1] = tmp;
        }
    }
	find_segment(isi->cols, dt.num_columns, col_threshold, isi->col_seg);
    isi->iterations++;
    if (detection_mode == MODE_10_FINGERS_ONLY){
        find_fingers_mode_10_only(isi);
    }else if (detection_mode == MODE_2_FINGERS_ONLY){
        find_fingers_mode_2_only(isi);
    }else if (detection_mode == MODE_2_FINGERS_EXT){
        find_fingers_mode_2_ext(isi);
    }
}

int * getFingers(int user){
    int *tab;
    lnode_t *node= NULL, *nil= NULL;
    int i = 0;

 	tab = (int *) malloc(sizeof(int)*3*is[user].nb_finger);

	node = ((lnode_t *)&(is[user].list_finger)->list_nilnode)->list_next;
	nil = &(is[user].list_finger)->list_nilnode;
	while (node != nil && i<(3*is[user].nb_finger)) {
		tab[i] = ((point_dt *)node->list_data)->x;
        i++;
		tab[i] =((point_dt *)node->list_data)->y;
		i++;
		tab[i] =((point_dt *)node->list_data)->id;
		i++;
		node = node->list_next;
    }
    //printf("==|> %d",tab[0]);
    return tab;
}

int * getColumns(int user){
    int *tab;
    int i;
    tab = (int *) malloc(sizeof(int)*dt.num_columns);
    for (i = 0; i< dt.num_columns; i++){
        tab[i]=is[user].cols[i];
    }
    return tab;
}

int * getRows(int user){
    int *tab;
    int i;
    tab = (int *) malloc(sizeof(int)*dt.num_rows);
    for (i = 0; i< dt.num_rows; i++){
        tab[i]=is[user].rows[i];
    }
    return tab;
}

int getFingersNumb(int user){
    return is[user].nb_finger;
}

int get_cols_thresold(){
    return col_threshold;
}

int get_rows_thresold(){
    return row_threshold;
}

void set_cols_thresold(int t){
    col_threshold = t;
}

void set_rows_thresold(int t){
    row_threshold = t;
}

void set_resolution_scale(int s){
 res_scale = s;
}

int get_resolution_scale(){
    return res_scale;
}

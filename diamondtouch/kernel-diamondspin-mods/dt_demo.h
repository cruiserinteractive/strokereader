#ifndef _DT_DEMO_H
#define _DT_DEMO_H


#include <gtk/gtk.h>
#include <gdk/gdk.h>


void usage();

void reader_func(void* data, int source, GdkInputCondition condition);

#endif

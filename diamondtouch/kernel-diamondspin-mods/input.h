#ifndef _INPUT_H
#define _INPUT_H

#include "../list.h"
#include "dt_eeparse.h"

typedef int   boolean;
#define	    FALSE	(0)
#define	    TRUE	(!FALSE)

#define     MAX_INPUTS (4)

#define     DEV_FNAME "/dev/DiamondTouch"

/* default thresholds */
#define     COL_THRESHOLD               60
#define     ROW_THRESHOLD               60
#define     MODE_2_FINGERS_ONLY         0 //can detect 0 to 2 fingers per user ... nothing else
#define     MODE_2_FINGERS_EXT          1 //can detect 0 to 2 fingers and some others paterns
#define     MODE_10_FINGERS_ONLY        2 // can detect 0 to 10 fingers per user ... nothing else

#define     NO_TOUCH_0_TO_0             0
#define     FINGER_APPEAR_0_TO_1        1
#define     FINGER_APPEAR_1_TO_2        2
#define     FINGER_MOVE_1_TO_1          3
#define     FINGER_MOVE_2_TO_2          4
#define     FINGER_DISAPPEAR_1_TO_0     5
#define     FINGER_DISAPPEAR_2_TO_1     6
#define     FINGER_DISAPPEAR_2_TO_0     7

extern unsigned char eebuff[0x40];
extern struct dt_eeparse dt;
extern char *dt_device_name;

extern int row_threshold;
extern int col_threshold;
extern int res_scale;

extern unsigned char *buff;

/* struct for each user */

typedef struct input_set {
  /* reader stuff */
  int number;
  int iterations;

  /* calc stuff */
  unsigned char *rows, *cols;//, *sdrv;
  list_t *row_seg, *col_seg;
  list_t *list_finger;
  int nb_finger;
  int state;

} input_set;

extern struct input_set is[MAX_INPUTS];


int init(int mode);
int init_input(char *devname);
int read_data(int source);
//int setup_input(char *devname);
int start_plateform() ;
void process_data(struct input_set *is);
int * getFingers(int user);
int * getColumns(int user);
int * getRows(int user);
int getFingersNumb(int user);
int get_cols_thresold();
int get_rows_thresold();
void set_cols_thresold(int t);
void set_rows_thresold(int t);
void set_resolution_scale(int s);
int get_resolution_scale();

#endif

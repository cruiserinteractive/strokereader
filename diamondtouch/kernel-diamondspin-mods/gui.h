

#ifndef _GUI_H
#define _GUI_H

#include <gtk/gtk.h>
#include <glade/glade.h>

#include "callbacks.h"


#define SIGNAL_STRENGTH_HEIGHTS 256
#define SCALE 8

int midd_x, midd_y;

G_MODULE_EXPORT gboolean on_drawing_area_expose_event(GtkWidget *widget, GdkEventExpose *event, gpointer user_data);
void setup_widgets(GladeXML *xml);
void recycle_display(void);
void display_data(struct input_set *is);
void init_display(int argc, char *argv[]);

#endif

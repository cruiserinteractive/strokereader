#ifndef _CLIENT_VISION_H
#define _CLIENT_VISION_H

#include	<stdio.h>
#include	<sys/types.h>
#include	<sys/socket.h>
#include	<netinet/in.h>
#include	<arpa/inet.h>


#define	SERV_UDP_PORT	52170
#define SERV_HOST_ADDR	"0.0.0.0"//"barsac.limsi.fr"	/* host addr for server */


void init_vision();
void *start_vision (void *arg);



#endif

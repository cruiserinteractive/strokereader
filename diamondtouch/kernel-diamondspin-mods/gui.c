
 #include "gui.h"

static GdkPixmap *middle_pixmap = NULL;

static GtkWidget *w_midd = NULL;

GdkFont* f;

#define CROSS_RADIUS (15)


/** define display colors **/
const char *user_color_strings[MAX_INPUTS] = {"magenta", "cyan", "green", "yellow" };

const char *scale_color_string = "gray25";
const char *threshold_color_string = "gray75";

GdkColor black_color = { 0x0000, 0x0000, 0x0000, 0x0000 };
GdkColor white_color = { 0x0000, 0xffff, 0xffff, 0xffff };

static GdkGC *pens[MAX_INPUTS];
static GdkGC *scale_pen = NULL;
static GdkGC *threshold_pen = NULL;


 GdkGC *get_pen(GdkPixmap *pixmap, const char *color_string) {
  GdkGC *gc;
  GdkColor color;
  gc = gdk_gc_new(pixmap);

  gdk_color_parse(color_string, &color);
  gdk_colormap_alloc_color(gdk_colormap_get_system(),
			   &color, FALSE, FALSE);

  gdk_gc_set_foreground(gc, &color);
  gdk_gc_set_fill(gc, GDK_SOLID);
  return gc;
}

void init_display(int argc, char *argv[]){
 	GladeXML *xml;
	gtk_init(&argc, &argv);
  	xml = glade_xml_new("dt_demo.glade", NULL, NULL);

  	/* connect signal handlers */
  	glade_xml_signal_autoconnect(xml);

	fprintf(stderr, "Starting DT tracker.....................");
  	setup_widgets(xml);
	fprintf(stderr, ".....................done\n");
   	gtk_main();

}


void setup_widgets(GladeXML *xml) {
  int i;

  w_midd = glade_xml_get_widget(xml, "drawing_area");

  midd_x = dt.num_columns * SCALE;
  midd_y = dt.num_rows * SCALE;

  gtk_widget_set_size_request(w_midd, midd_x, midd_y);

  if (middle_pixmap) gdk_pixmap_unref(middle_pixmap);
  middle_pixmap = gdk_pixmap_new(w_midd->window, midd_x, midd_y, -1);
  gdk_draw_rectangle(middle_pixmap, w_midd->style->black_gc, TRUE, 0, 0, midd_x, midd_y);

  // allocate GCs for our colors
  for (i = 0; i < MAX_INPUTS; i++)
    pens[i] = get_pen(middle_pixmap, user_color_strings[i]);

  scale_pen = get_pen(middle_pixmap, scale_color_string);
  threshold_pen = get_pen(middle_pixmap, threshold_color_string);

  f = gdk_font_load ("-adobe-helvetica-bold-r-normal--12-120-75-75-p-70-iso8859-1");
}

G_MODULE_EXPORT gboolean on_drawing_area_expose_event(GtkWidget *widget, GdkEventExpose *event, gpointer user_data) {
  gdk_draw_pixmap(widget->window,
		  widget->style->fg_gc[GTK_WIDGET_STATE(widget)],
		  middle_pixmap,
		  event->area.x, event->area.y,
		  event->area.x, event->area.y,
		  event->area.width, event->area.height);

  return FALSE;
}

/* draw the offscreen pixmaps to the screen and then erase them */
void recycle_display() {
  GdkRectangle ur;

  // draw the pixmaps to the screen

  // middle pixmap
  ur.x = ur.y = 0;
  ur.width = w_midd->allocation.width;
  ur.height = w_midd->allocation.height;
  gtk_widget_draw(w_midd, &ur);
  // erase the pixmaps

  // middle
  gdk_draw_rectangle(middle_pixmap, w_midd->style->black_gc,TRUE, 0, 0,w_midd->allocation.width,w_midd->allocation.height);
}

void display_data(struct input_set *is) {

	lnode_t *node= NULL, *nil= NULL;
 	int i, x =0 ,y = 0;
 	char id[4];
	int space = (int)((SCALE-MAX_INPUTS)/2);

	// affichage des doigts
	node = ((lnode_t *)&(is->list_finger)->list_nilnode)->list_next;
	nil = &(is->list_finger)->list_nilnode;
	while (node != nil) {
		x = (((point_dt *)node->list_data)->x * SCALE) + space + is->number;
		y = (((point_dt *)node->list_data)->y * SCALE) + space + is->number;
		gdk_draw_arc (middle_pixmap, pens[is->number], TRUE, x-CROSS_RADIUS, y-CROSS_RADIUS, 2*CROSS_RADIUS, 2*CROSS_RADIUS, 0, 23040);
		//gdk_draw_line (middle_pixmap, pens[is->number], ((point_dt *)node->list_data)->x, ((point_dt *)node->list_data)->y, ((point_dt *)node->list_data)->x+2*((point_dt *)node->list_data)->vx, ((point_dt *)node->list_data)->y+2*((point_dt *)node->list_data)->vy);
		sprintf(id,"%d",((point_dt *)node->list_data)->id);
		gdk_draw_string (middle_pixmap,f,scale_pen,x, y+4,id);
		node = node->list_next;
	}

	// draw the row and column histograms
	// columns

	for (i = 0; i < dt.num_columns; i++) {
		if (dt.columns_left_to_right)
			gdk_draw_rectangle(middle_pixmap, pens[is->number], TRUE, SCALE * i + space + is->number, 0, 1, is->cols[i]);
		else
			gdk_draw_rectangle(middle_pixmap, pens[is->number], TRUE, midd_x - (SCALE * i + space + is->number), 0, 1, is->cols[i]);
	}
	// rows
	for (i = 0; i < dt.num_rows; i++) {
		if (dt.rows_top_to_bottom)
			gdk_draw_rectangle(middle_pixmap, pens[is->number], TRUE, 0,  SCALE * i + space + is->number, is->rows[i], 1);
		else
			gdk_draw_rectangle(middle_pixmap, pens[is->number], TRUE,  0, midd_y - ( SCALE * i + space + is->number), is->rows[i], 1);
	}
}

/*
Copyright 2003, 2004 Mitsubishi Electric Research Laboratories
All rights reserved

This file is part of the MERL DiamondTouch Linux device driver.

    The DiamondTouch Linux device driver is free software; you can
    redistribute it and/or modify it under the terms of the GNU General
    Public License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    The DiamondTouch Linux device driver is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the
    implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
    PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the DiamondTouch Linux device driver; if not, write to
    the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
    Boston, MA 02111-1307 USA

DiamondTouch is a trademark of Mitsubishi Electric Research Laboratories.
Linux is a trademark of Linus Torvalds.
*/


#include "diamondtouch.h"

#include <unistd.h>
#include <errno.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int col_threshold = COL_THRESHOLD;
int row_threshold = ROW_THRESHOLD;

unsigned char eebuff[0x40];
struct dt_eeparse dt;
unsigned char *buff;
int *seq;

struct input_set is[MAX_INPUTS];

int setup_input(char *devname) {
  int i, fd, ret;
  int rowscols;

  fd = open(devname, O_RDONLY | O_NONBLOCK);
  if (fd < 0) {
    fprintf(stderr, "Could not open device %s\n", devname);
    return fd;
  }

  ret = ioctl(fd, DT_IOCGPARAMS, eebuff);
  if (ret < 0) {
    perror("ioctl");
    fprintf(stderr, "ioctl failed with ret = %d\n", ret);
    return -2;
  }

  dt_parse_params(eebuff, &dt);

  rowscols = dt.num_rows + dt.num_columns;

  buff = (unsigned char *) malloc(dt.read_size);
  if (buff == NULL) {
    fprintf(stderr, "Could not allocate read buffer.\n");
    return -3;
  }
  seq = (int *) buff; /* point to the sequence number at the beginning */

  gdk_input_add (fd, GDK_INPUT_READ, reader_func, (gpointer) (is));

  for (i = 0; i < dt.num_users; i++) {
    is[i].iterations = 0;
    is[i].number = i;
    if (dt.rows_first) {
      is[i].rows = buff + 4 + i * rowscols;
      is[i].cols = buff + 4 + i * rowscols + dt.num_rows;
    }
    else {
      is[i].rows = buff + 4 + i * rowscols + dt.num_columns;
      is[i].cols = buff + 4 + i * rowscols;
    }
  }
  return 0;
}

int reader_func(struct input_set *data) {
  int ret, i;
  struct input_set *is = (struct input_set *) data;

  ret = read(source, buff, dt.read_size);
  if (ret < 0) {
    if (errno == EAGAIN) {
      fprintf(stderr, "reader func would block -- ??\n");
      return 0;
    }
    else {
        perror("::read error: ");
      return -1;
    }
  }
  if (ret != dt.read_size) {
      fprintf(stderr, "read returned %d instead of %d\n", ret, dt.read_size);
      return -2;
  }

  for (i = 0; i < dt.num_users; i++) {
    process_data(is + i);
    //display_data(is + i);
  }
  //recycle_display();
  return 1;
}

/* calculations to perform on the input data */

void process_data(struct input_set *is) {
  is->maxrow = max_row(is->rows);
  is->maxcol = max_col(is->cols);
  row_bounding_box(is->rows, &is->bbrowmin, &is->bbrowmax);
  col_bounding_box(is->cols, &is->bbcolmin, &is->bbcolmax);
  is->iterations++;
}


int max_row(unsigned char *rc) {
  int i, mx, ret;

  ret = mx = -1;
  for (i = 0; i < dt.num_rows; i++) {
    if (rc[i] > mx) {
      mx = rc[i];
      ret = i;
    }
  }
  if (mx < row_threshold) ret = -1;
  return ret;
}

int max_col(unsigned char *rc) {
  int i, mx, ret;

  ret = mx = -1;
  for (i = 0; i < dt.num_columns; i++) {
    if (rc[i] > mx) {
      mx = rc[i];
      ret = i;
    }
  }
  if (mx < col_threshold) ret = -1;
  return ret;
}

void row_bounding_box(unsigned char *rc, int *mn, int *mx) {
  int i;

  *mn = *mx = -1;

  for (i = 0; i < dt.num_rows; i++) {
    if (rc[i] >= row_threshold) {
      if (*mn == -1) *mn = i;
      *mx = i;
    }
  }
}

void col_bounding_box(unsigned char *rc, int *mn, int *mx) {
  int i;

  *mn = *mx = -1;

  for (i = 0; i < dt.num_columns; i++) {
    if (rc[i] >= col_threshold) {
      if (*mn == -1) *mn = i;
      *mx = i;
    }
  }
}

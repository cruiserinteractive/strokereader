#ifndef _DIAMONDTOUCH_H
#define _DIAMONDTOUCH_H

/*
Copyright 2003, 2004 Mitsubishi Electric Research Laboratories
All rights reserved

This file is part of the MERL DiamondTouch Linux device driver.

    The DiamondTouch Linux device driver is free software; you can
    redistribute it and/or modify it under the terms of the GNU General
    Public License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    The DiamondTouch Linux device driver is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the
    implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
    PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the DiamondTouch Linux device driver; if not, write to
    the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
    Boston, MA 02111-1307 USA

DiamondTouch is a trademark of Mitsubishi Electric Research Laboratories.
Linux is a trademark of Linus Torvalds.
*/

//#include <linux/fs.h>
#include <linux/types.h>
#include <linux/ioctl.h>

#define VENDID	0x06d3
#define PRODID	0x0f81

/* ioctl definitions */

#define DT_IOC_MAGIC  0xdd

#define DT_IOCGPARAMS _IOC(_IOC_READ,  DT_IOC_MAGIC, 1, 0x40)
#define DT_IOCSPARAMS _IOC(_IOC_WRITE, DT_IOC_MAGIC, 2, 0x40)

/* eeprom data format (little-endian, and packed) */
struct eeprom_data {
  __u8  boot_byte;
  __u16 vendor_id;
  __u16 product_id;
  __u16 device_id;
  __u8  configuration_byte;
  __u8  reserved_byte;
  __u8  eeprom_version;
  __u16 renum_vendor_id;
  __u16 renum_product_id;
  __u16 renum_device_id;
  __u8  calibration_constant[8];
  __u8  pic_firmware_version;
  __u8  num_users;
  __u16 num_cols;
  __u16 num_rows;
  __u16 update_period;
  __u16 col_width;
  __u16 row_width;
  __u8  orientation_byte;
  __u8  spare0;
  __u8  spare1;
  __u8  spare2;
  __u8  data_string[8];
} __attribute ((packed));

#define DT_ROWS_FIRST		0x01
#define DT_LEFT_TO_RIGHT	0x02
#define DT_BOTTOM_TO_TOP	0x04

#ifdef __KERNEL__

#include <linux/proc_fs.h>
#include <asm/semaphore.h>

/* these are constants because of current hardware constraints */
#define OUT_BUFFSIZE	0x40
#define PARAM_BUFFSIZE	0x40
#define USER_BUFFSIZE	0x40
#define BUFF_HEADERSIZE	(4)
#define MAX_DEVICES	(8)

/* this is the per-user data for the device */
struct dt_per_user {
  struct urb *irq_urb;
  unsigned dudata_subframe;
  u8 userbuf[USER_BUFFSIZE];
  volatile u8 * volatile dudata;
};

/* this is the data for each instance of diamondtouch device */
struct dt_per_device {
  struct semaphore sem;
  struct usb_device *udev;
  unsigned active;
  unsigned moribund;

  /* device parameters */
  u8 param_buff[PARAM_BUFFSIZE];
  unsigned num_users;
  unsigned read_size;
  unsigned num_samples;
  unsigned num_subframes;
  unsigned last_payload;
  unsigned all_subframes_received;
  unsigned frame_incer;
  unsigned minor;
  
  /* Bulk Transfers */
  u8 out_buff[OUT_BUFFSIZE];

  /* Interrupt Transfers */
  wait_queue_head_t irq_q;
  unsigned interval;

  /* device user data */
  struct dt_per_user *user; /* must kmalloc these */
  unsigned dudata_unread;
  unsigned dudata_frame;
  u8 *dbuff; /* the data buffers (ptr for mallocing and freeing) */
  volatile u8 * volatile fbuff; /* buffer that is currently being filled */
  volatile u8 * volatile ubuff; /* ultimate buffer (latest data) */
  volatile u8 * volatile pbuff; /* penultimate buffer (next-to-latest data) */
  volatile long latest_data_available;

  /* list of per-open data for this device */
  struct list_head per_open_list;
  int open_count;
};

/* this is the per-open data */
struct dt_per_open {
  struct list_head list;
  long last_data_sent;
  struct dt_per_device *dtpd;
};


/* forward declarations */
static void dt_irq_start(struct dt_per_device *dt);
static void dt_irq_stop(struct dt_per_device *dt);
static struct file_operations dt_file_operations;
static void dt_irq_irq(struct urb *murb);
static int dt_parse_params(struct dt_per_device *dt);
static void init_minors(void);
static void free_minor(struct dt_per_device *dt);
static int map_minor(struct dt_per_device *dt);
static void rotate_buffers(struct dt_per_device *dt);
static int read_proc_dt(char *buf, char **start, off_t offset,
			int len, int *eof, void *data);


#define DT_STANDARD_TIMEOUT	(HZ/2)

/* pipe definitions */
#define DT_USER1_PIPE	(1)
#define DT_USER2_PIPE	(2)
#define DT_USER3_PIPE	(3)
#define DT_USER4_PIPE	(4)
#define DT_OUT_PIPE	(5)
#define DT_PARAM_PIPE	(6)

#define DT_FIRST_PACKET_PAYLOAD		(58)
#define DT_SUBSEQUENT_PACKET_PAYLOAD	(62)

#endif /* __KERNEL__ */
#endif /* _DIAMONDTOUCH_H */

/*
Copyright 2003, 2004 Mitsubishi Electric Research Laboratories
All rights reserved

This file is part of the MERL DiamondTouch Linux device driver.

    The DiamondTouch Linux device driver is free software; you can
    redistribute it and/or modify it under the terms of the GNU General
    Public License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    The DiamondTouch Linux device driver is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the
    implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
    PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the DiamondTouch Linux device driver; if not, write to
    the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
    Boston, MA 02111-1307 USA

DiamondTouch is a trademark of Mitsubishi Electric Research Laboratories.
Linux is a trademark of Linus Torvalds.
*/

#ifndef _INPUT_H
#define _INPUT_H

#include <stdio.h>
#include <gtk/gtk.h>

#include "dt_eeparse.h"

#define MAX_INPUTS (8)

#define DEV_FNAME "/dev/DiamondTouch"

/* default thresholds */
#define COL_THRESHOLD (60)
#define ROW_THRESHOLD (60)

extern struct dt_eeparse dt;

/* struct for each user */

struct input_set {
  /* reader stuff */
  int number;
  int iterations;

  /* calc stuff */
  unsigned char *rows, *cols;
  int maxrow, maxcol;
  int bbrowmin, bbrowmax, bbcolmin, bbcolmax;
};

int setup_input(char *devname);
void reader_func(gpointer data, gint source, GdkInputCondition condition);
int max_col(unsigned char *rc);
int max_row(unsigned char *rc);
void row_bounding_box(unsigned char *rc, int *mn, int *mx);
void col_bounding_box(unsigned char *rc, int *mn, int *mx);
void process_data(struct input_set *is);
void do_display(struct input_set *is);

#endif

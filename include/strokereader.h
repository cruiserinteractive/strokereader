/* $Id$ $URL$ */
#ifndef STROKEREADER_DOT_AITCH
#define STROKEREADER_DOT_AITCH

/**\file strokereader.h
 * Definition of the MimioEvent class and Mimio event
 * initialisers and event retrievers.
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#ifdef _MSC_VER
#ifdef STROKEREADER_EXPORTING
#define STROKEREADER_API __declspec(dllexport)
#else
#define STROKEREADER_API __declspec(dllimport)
#endif
#else
#define STROKEREADER_API //nothing
#endif

#include <memory>
#if defined __GNUC__ && __GNUC__ >= 4
#include <ext/pool_allocator.h>
#warning "Using GNU Pool Allocator (extension)"
#define FAST_ALLOCATOR __gnu_cxx::__pool_alloc
#else
#ifdef _MSC_VER
#pragma message("Not GNU, using vanilla allocator")
#else
#warning "Not GNU, using vanilla allocator"
#endif
#define FAST_ALLOCATOR std::allocator
#endif

/** A raw event from the Mimio pen system (i.e. in
 * Mimio coordinates -- like (x, y) in [(0,0), (10000,5000)] )
 */
class STROKEREADER_API MimioEvent {
    /** the allocator for all MimioEvent */
    static FAST_ALLOCATOR<MimioEvent> alloc;
public:
    /** The state of the pen for this point */
    enum PENSTATE {
        NOTHING,    ///< Unknown (or a signal message)
        UP,         ///< We were just lifted
        DOWN,       ///< We were just placed down
        MOVING      ///< We moved
    };
    PENSTATE state; ///< The state of the pen
    int x;          ///< x mimio-coordinate
    int y;          ///< y mimio-coordinate
    int user;       ///< The "user" -- which pen was used

    /** Brain-dead constructor */
    MimioEvent(PENSTATE ps = NOTHING, int xx = 0, int yy = 0, int uuser = 0)
        :
    state(ps), x(xx), y(yy), user(uuser)
    {}

    /** Allocate with the allocator */
    void* operator new(size_t) {return alloc.allocate(1);}
    /** Deallocate with the allocator */
    void operator delete(void* p, size_t) {
        alloc.deallocate(static_cast<MimioEvent*>(p), 1);
    }
};

STROKEREADER_API void strokereader_static_init();

/** Wait (block) until the StrokeSender connects */
STROKEREADER_API void waitForMimioConnect();
/** Returns true if we are connected to a StrokeSender */
STROKEREADER_API bool isMimioConnected();
/** Disconnect from the StrokeSender and close sockets */
STROKEREADER_API void cleanupMimioReader();
/**
 * Start the thread that waits for StrokeSender connections and
 * starts pumping events into the SDL event queue. We also try
 * to signal the StrokeSender plugin to re-activate and reconnect
 * with a TCP connect request.
 *
 * \param host The host to signal to connect to us
 * \param ports_to_try how many ports past STROKE_PORT to attempt to signal
 * \param retries how many times to go through ports_to_try
 */
STROKEREADER_API bool startMimioServer(const char* host = "127.0.0.1", unsigned ports_to_try = 10, int retries = 1);

/** Returns the most recent mimio event if it exists and clear the event queue.
 * If there have been no events or if it has already been returned, we return NULL
 */
STROKEREADER_API MimioEvent *pollRecentMimio();

/** Returns the oldest mimio event that has not yet been returned if it exists.
 * If all have been returned or if there are no events, we return NULL.
 */
STROKEREADER_API MimioEvent *pollOldMimio();

/** Return the most recent mimio event, blocking until it exists,
 * then clear the event queue
 */
STROKEREADER_API MimioEvent *waitRecentMimio();

/** Return the oldest mimio event not yet returned, blocking until one exists */
STROKEREADER_API MimioEvent *waitOldMimio();

/**
 * Callback type when auto calibrating input events.
 * \note MimioEvent:x and MimioEvent:y will be calibrated, based on the loaded network.dat
 */
typedef void (*AUTOCALIBRATE_CALLBACK)(MimioEvent *);

/** This is called if we want to hide the cursor once the first event is received */
typedef void (*HIDE_CURSOR_CALLBACK)();

/**
 * Start an auto-calibrator that uses a callback. It begins by clearing waiting events,
 * and then it will wait() for mimio events.
 *
 * \param callback the callback
 * \param screen_width the screen width, for calibration
 * \param screen_height the screen height, for calibration
 * \param host_or_path the hostname of the Mimio StrokeSender OR the absolute path of the mimio device node
 * \param hide_cursor the function called after the first mimio event (if specified)
 * \param event_number_tracker a tracker for the number of events waiting to be processed
 * \param max_event_backlog if *event_number_tracker is greater than this number, we don't
 *                           call the callback -- we just discard the event (but only if it
 *                           is a move event)
 * \param network_dat the file from which to load the calibration data
 */
STROKEREADER_API bool startAutoCalibrator(AUTOCALIBRATE_CALLBACK callback,
                         unsigned screen_width,
                         unsigned screen_height,
                         const char* host_or_path = "127.0.0.1",
                         HIDE_CURSOR_CALLBACK hide_cursor = 0,
                         unsigned int* event_number_tracker = 0,
                         unsigned int  max_event_backlog = 1,
                         const char* network_dat = "network.dat");

/**
 * Cleanup for automatic calibrator -- closes sockets and frees resources.
 */
STROKEREADER_API void cleanupAutoCalibrator();

#endif


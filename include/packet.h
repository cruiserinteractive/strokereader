/* $Id$ $URL$ */
#ifndef PACKET_DOT_AITCH
#define PACKET_DOT_AITCH

/**\file packet.h
 * Declaration of MimioPacket
 */

/** Packet structure receieved from MSVC StrokeSender */
struct MimioPacket {
    /** The pen actions */
    enum ACTION {NONE = 0, UP = 1, DOWN = 2, MOVE = 3};
    long user;    ///< The user (pen ID)
    short x;      ///< The (Mimio) coordinates (x)
    short y;      ///< The (Mimio) coordinates (y)
    short action; ///< The action
};

#endif

/* $Id$ $URL$ */
#ifndef SCHEDLOCK_DOT_AITCH
#define SCHEDLOCK_DOT_AITCH

/**\file schedlock.h
 * Thread controller to starve out drawing threads
 * until all mimio events are processed
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * $Revision$
 * $Date$
 */

#ifdef _WIN32

/**
 * Starve the thread that calls this function until *all*
 * Mimio events that we know about have been fully processed.
 * i.e. until get_num_mimio_events() would return 0.
 */
void starve_for_mimio();


/**
 * Increase the number of Mimio events we know about. i.e.
 * as soon as we have received one over a socket.
 */
void mimio_incr();

/** Decrement the number of Mimio events we know about. i.e.
 * once an event has been fully processed.
 */
void mimio_decr();

/** Retrieve the value of the mimio event counter */
int get_num_mimio_events();

#else
inline void starve_for_mimio() {}
inline void mimio_incr() {}
inline void mimio_decr() {}
inline int get_num_mimio_events() {return 0;}
#endif

#endif

